/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seller;
import datastore.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JFileChooser;

/**
 *
 * @author PaulAsyn
 */
public class MainGUI extends javax.swing.JFrame {

    DBST dbst;

    public MainGUI() {
        dbst = new DBST();
        initComponents();
        this.setLocationRelativeTo(null);
        this.setTitle("Java Beans Wholesale - Dinosaur Bone Seller Tool");
        this.setSize(3600, 1800);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jDialog1 = new javax.swing.JDialog();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        MainMenuBar = new javax.swing.JMenuBar();
        fileMainMenuItem = new javax.swing.JMenu();
        LoadMainMenuItem = new javax.swing.JMenu();
        LoadMapMenuItem = new javax.swing.JMenuItem();
        LoadSellerMenuItem = new javax.swing.JMenuItem();
        LoadDinoBonesMenuItem = new javax.swing.JMenuItem();
        SaveMainMenuItem = new javax.swing.JMenu();
        SaveMapMenuItem = new javax.swing.JMenuItem();
        SaveSellerMainMenuItem = new javax.swing.JMenuItem();
        SaveBonesMainMenuItem = new javax.swing.JMenuItem();
        fileExitMenuItem = new javax.swing.JMenuItem();
        sellerMainMenuItem = new javax.swing.JMenu();
        sellerCreateSellerMenuItem = new javax.swing.JMenuItem();
        sellerUpdateSellerMenuItem = new javax.swing.JMenuItem();
        sellerRemoveSellerMenuItem = new javax.swing.JMenuItem();
        dinosaurHandlingMainMenu = new javax.swing.JMenu();
        createDinosaurMenuItem = new javax.swing.JMenuItem();
        updateDinosaurMenuItem = new javax.swing.JMenuItem();
        removeDinosaurMenuItem = new javax.swing.JMenuItem();
        oneLineCreateDinosaurMenuItem = new javax.swing.JMenuItem();
        AboutMainMenuItem = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        jMenu2.setText("File");
        jMenuBar1.add(jMenu2);

        jMenu3.setText("Edit");
        jMenuBar1.add(jMenu3);

        jMenu4.setText("jMenu4");

        jMenuItem4.setText("jMenuItem4");

        jMenuItem6.setText("jMenuItem6");

        jMenuItem7.setText("jMenuItem7");

        jTextField1.setText("        Loaded.");
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(59, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(0, 56, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        fileMainMenuItem.setText("File");

        LoadMainMenuItem.setText("Load");

        LoadMapMenuItem.setText("Load Map");
        LoadMapMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoadMapMenuItemActionPerformed(evt);
            }
        });
        LoadMainMenuItem.add(LoadMapMenuItem);

        LoadSellerMenuItem.setText("Load Seller");
        LoadMainMenuItem.add(LoadSellerMenuItem);

        LoadDinoBonesMenuItem.setText("Load Dinosaur Bones");
        LoadMainMenuItem.add(LoadDinoBonesMenuItem);

        fileMainMenuItem.add(LoadMainMenuItem);

        SaveMainMenuItem.setText("Save");

        SaveMapMenuItem.setText("Save Map");
        SaveMainMenuItem.add(SaveMapMenuItem);

        SaveSellerMainMenuItem.setText("Save Seller");
        SaveSellerMainMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveSellerMainMenuItemActionPerformed(evt);
            }
        });
        SaveMainMenuItem.add(SaveSellerMainMenuItem);

        SaveBonesMainMenuItem.setText("Save Bones");
        SaveMainMenuItem.add(SaveBonesMainMenuItem);

        fileMainMenuItem.add(SaveMainMenuItem);

        fileExitMenuItem.setText("Exit");
        fileExitMenuItem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fileExitMenuItemMouseClicked(evt);
            }
        });
        fileExitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileExitMenuItemActionPerformed(evt);
            }
        });
        fileMainMenuItem.add(fileExitMenuItem);

        MainMenuBar.add(fileMainMenuItem);

        sellerMainMenuItem.setText("Seller");

        sellerCreateSellerMenuItem.setText("Create Seller");
        sellerCreateSellerMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sellerCreateSellerMenuItemActionPerformed(evt);
            }
        });
        sellerMainMenuItem.add(sellerCreateSellerMenuItem);

        sellerUpdateSellerMenuItem.setText("Update Seller");
        sellerUpdateSellerMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sellerUpdateSellerMenuItemActionPerformed(evt);
            }
        });
        sellerMainMenuItem.add(sellerUpdateSellerMenuItem);

        sellerRemoveSellerMenuItem.setText("Remove Seller");
        sellerMainMenuItem.add(sellerRemoveSellerMenuItem);

        MainMenuBar.add(sellerMainMenuItem);

        dinosaurHandlingMainMenu.setText("Dinosaur Handling");

        createDinosaurMenuItem.setText("Create Dinosaur Bone");
        createDinosaurMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createDinosaurMenuItemActionPerformed(evt);
            }
        });
        dinosaurHandlingMainMenu.add(createDinosaurMenuItem);

        updateDinosaurMenuItem.setText("Update Dinosaur Bone");
        updateDinosaurMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateDinosaurMenuItemActionPerformed(evt);
            }
        });
        dinosaurHandlingMainMenu.add(updateDinosaurMenuItem);

        removeDinosaurMenuItem.setText("Remove Dinosaur Bone");
        dinosaurHandlingMainMenu.add(removeDinosaurMenuItem);

        oneLineCreateDinosaurMenuItem.setText("Quick Create Bone");
        oneLineCreateDinosaurMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oneLineCreateDinosaurMenuItemActionPerformed(evt);
            }
        });
        dinosaurHandlingMainMenu.add(oneLineCreateDinosaurMenuItem);

        MainMenuBar.add(dinosaurHandlingMainMenu);

        AboutMainMenuItem.setText("About");

        aboutMenuItem.setText("About This Project");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        AboutMainMenuItem.add(aboutMenuItem);

        MainMenuBar.add(AboutMainMenuItem);

        setJMenuBar(MainMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 853, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 553, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void createDinosaurMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createDinosaurMenuItemActionPerformed
        CreateDinosaurMenuItem cdbmi;
        cdbmi = new CreateDinosaurMenuItem(this, true);
        cdbmi.setVisible(true);
        this.setLocationRelativeTo(null);
        
    }//GEN-LAST:event_createDinosaurMenuItemActionPerformed

    private void fileExitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileExitMenuItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fileExitMenuItemActionPerformed

    private void updateDinosaurMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateDinosaurMenuItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updateDinosaurMenuItemActionPerformed

    private void fileExitMenuItemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fileExitMenuItemMouseClicked
                // TODO add your handling code here:
    }//GEN-LAST:event_fileExitMenuItemMouseClicked

    private void sellerCreateSellerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sellerCreateSellerMenuItemActionPerformed
        createSellerDialogBox csdg;        // TODO add your handling code here:
        csdg = new createSellerDialogBox(this,true);
        csdg.setDbst(dbst);
        csdg.setVisible(true);
        
    }//GEN-LAST:event_sellerCreateSellerMenuItemActionPerformed

    private void SaveSellerMainMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveSellerMainMenuItemActionPerformed

        JFileChooser sellersaver= new JFileChooser(".");
    
        int result = sellersaver.showDialog(this, "Save Seller");
        
        if( result == 0)
        { 
        dbst.SaveSeller(sellersaver.getSelectedFile());
        }           
        
    }//GEN-LAST:event_SaveSellerMainMenuItemActionPerformed

    private void LoadMapMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoadMapMenuItemActionPerformed
 JFileChooser loader = new JFileChooser(".");

        int result = loader.showDialog(this, "Load Map");

        if( result == 0){

            File mapFile= loader.getSelectedFile();
            String line=null;
            String dataset[];
            int row;
            int col;
            int land;

            try
            (Scanner mapReader = new Scanner(mapFile)) {

                while (mapReader.hasNextLine())
                {
                    line=mapReader.nextLine();
                    //Data here screen.format(line);
                    dataset=line.split(",");
                    col=Integer.parseInt(dataset[0]);
                    row=Integer.parseInt(dataset[1]);
                    land=Integer.parseInt(dataset[2]);
                    dbst.LoadMap(col, row, land);
                }
            }
            catch (FileNotFoundException fnfE)
            {
            }
            
            
            ImageComponent newImage = new ImageComponent();
            
            
        }        // TODO add your handling code here:
    }//GEN-LAST:event_LoadMapMenuItemActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
       
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        aboutDialog aboutPan;        // TODO add your handling code here:
        aboutPan = new aboutDialog(this,true);
        aboutPan.setVisible(true);  
        aboutPan.setDbst(dbst);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void sellerUpdateSellerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sellerUpdateSellerMenuItemActionPerformed
        updateSellerDialogBox updateSellerBox;
        updateSellerBox = new updateSellerDialogBox(this, true);
        updateSellerBox.setVisible(true);
        updateSellerBox.setDbst(dbst);
    }//GEN-LAST:event_sellerUpdateSellerMenuItemActionPerformed

    private void oneLineCreateDinosaurMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oneLineCreateDinosaurMenuItemActionPerformed
        quickCreateBoneDialog quickBone;
        quickBone = new quickCreateBoneDialog(this, true);
        quickBone.setVisible(true);
        quickBone.setDbst(dbst);
    
    }//GEN-LAST:event_oneLineCreateDinosaurMenuItemActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu AboutMainMenuItem;
    private javax.swing.JMenuItem LoadDinoBonesMenuItem;
    private javax.swing.JMenu LoadMainMenuItem;
    private javax.swing.JMenuItem LoadMapMenuItem;
    private javax.swing.JMenuItem LoadSellerMenuItem;
    private javax.swing.JMenuBar MainMenuBar;
    private javax.swing.JMenuItem SaveBonesMainMenuItem;
    private javax.swing.JMenu SaveMainMenuItem;
    private javax.swing.JMenuItem SaveMapMenuItem;
    private javax.swing.JMenuItem SaveSellerMainMenuItem;
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem createDinosaurMenuItem;
    private javax.swing.JMenu dinosaurHandlingMainMenu;
    private javax.swing.JMenuItem fileExitMenuItem;
    private javax.swing.JMenu fileMainMenuItem;
    private javax.swing.JButton jButton1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JMenuItem oneLineCreateDinosaurMenuItem;
    private javax.swing.JMenuItem removeDinosaurMenuItem;
    private javax.swing.JMenuItem sellerCreateSellerMenuItem;
    private javax.swing.JMenu sellerMainMenuItem;
    private javax.swing.JMenuItem sellerRemoveSellerMenuItem;
    private javax.swing.JMenuItem sellerUpdateSellerMenuItem;
    private javax.swing.JMenuItem updateDinosaurMenuItem;
    // End of variables declaration//GEN-END:variables
}
