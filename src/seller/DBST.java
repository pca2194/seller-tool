package seller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.Boolean;

//*Author(s) --- Java Beans ---
//*CSE1325-001
//*Resources 
//**Sample Code(s) via Professor Becker

public class DBST {

    Scanner input;
    Formatter output;
    char displayArray[][];
    char boneArray[][];
    char sellerArray[][];
    ArrayList <MapNode> mapArrayList;
    ArrayList <BoneNode> boneArrayList;
    ArrayList <Seller> sellerArrayList;
    
    int loaded=0;
    int loaded2=0;
    
    public DBST() {
        
        input=new Scanner(System.in);
        output=new Formatter(System.out);
        mapArrayList=new ArrayList<>(1200);
        displayArray=new char[60][20];//Columnn by Row
        
        boneArrayList=new ArrayList<>();
        boneArray=new char[6][20];
        
        sellerArrayList=new ArrayList<>();
        sellerArray=new char[3][1];
        
        
} //*setting some global variables under a class
    
    public void LoadMap(int col, int row, int land) //*loads the map off a text file into an array
    {
           MapNode tempNode;
            tempNode=new MapNode(col,row,land);
            mapArrayList.add(tempNode);
    
    }
    
    public void showMap() //*converts the array into a 2D array, then prints it
    {
        int mapLoop;
        int mapMax;
        MapNode tempNode;
        
        mapMax=mapArrayList.size();
        
        //Translate Array List to 2D Array
        for (mapLoop=0;mapLoop<mapMax;mapLoop++)
        {
            tempNode=mapArrayList.get(mapLoop);
            //screen.format("%d,%d,%d",tempNode.col,tempNode.row,tempNode.land);
        
            switch(tempNode.land)
            {
                case 0:
                    displayArray[tempNode.col][tempNode.row]='.';
                    break;
                case 1:
                    displayArray[tempNode.col][tempNode.row]='*';
                    break;
                     }
        }
        
        //Show the 2D array
        int rowLoop;
        int colLoop;
        char symbol;
        
        output.format("                  JavaBeans Bone WholeSale                  \n");
        
        for (rowLoop=0;rowLoop<20;rowLoop++)
        {
            for(colLoop=0;colLoop<60;colLoop++)
            {
                symbol=displayArray[colLoop][rowLoop];
                output.format("%c",symbol);
                
            }
            output.format("\n");
        }
        
        
        
    }    
    
    public void showMap2() //*a second showMap which is triggered if the Bone file has been loaded
    {
        //*Did a second showMap with a load trigger so that I didn't have to worry about 
        //*showMap failing if bones aren't loaded, but have bone values in the code.
        int mapLoop;
        int mapMax;
        int listMax;
        int key = 0;
        int counter;
        MapNode tempNode;
        BoneNode tempNode2;
        Seller tempNode3;
        
        listMax=boneArrayList.size();
        
        mapMax=mapArrayList.size();
        
        //Translate Array List to 2D Array
        for (mapLoop=0;mapLoop<mapMax;mapLoop++)
        {
            tempNode=mapArrayList.get(mapLoop);
            //screen.format("%d,%d,%d",tempNode.col,tempNode.row,tempNode.land);
        
            switch(tempNode.land)
            {
                case 0:
                    displayArray[tempNode.col][tempNode.row]='.';
                    break;
                case 1:
                    displayArray[tempNode.col][tempNode.row]='*';
                    break;
            }
        }
        //Show the 2D array
        int rowLoop;
        int colLoop;
        char symbol;

        output.format("                  JavaBeans Bone WholeSale                  \n");
        //*Check to see if bone coordinates matches with map coordinates, and if so, replaces with 'X' or '$' depending on sold value
        for (rowLoop=0;rowLoop<20;rowLoop++)
        {
            for(colLoop=0;colLoop<60;colLoop++)
            {
                if(loaded2 == 1)
                {
                    tempNode3=sellerArrayList.get(0);
                        if(rowLoop == tempNode3.rowIndex)
                        {
                            if(colLoop == tempNode3.colIndex)
                            {
                                key = 3;
                            }
                        }
                }
                for(counter=0;counter<listMax;counter++)
                {
                    tempNode2=boneArrayList.get(counter);
                    if(rowLoop == tempNode2.lad)
                    {
                        if(colLoop == tempNode2.lon)
                        {
                            if(tempNode2.sold == 0)
                            {
                                key = 1;
                            }
                            else
                            {
                                key = 2;
                            }
                        }
                    }
                    }
                
                if(key > 0)
                {
                    if(key == 1)
                        output.format("X");
                    else if(key == 2)
                        output.format("$");
                    else
                        output.format("S");
                }
                else
                {
                    symbol=displayArray[colLoop][rowLoop];
                    output.format("%c",symbol);
                }
                counter = 0;
                key = 0;
                
            }
            output.format("\n");
        }               
        
    } 
    
    public void Handle() //*initiates the menu for handling bones
    {
        String key;
        boolean done;
        
        done=false;
        
        while(!done)
        {
        
        output.format("What do you want to do?\n");
        output.format("1. Create a Dinosaur Bone.\n");
        output.format("2. Update a Dinosaur Bone.\n");
        output.format("3. Remove a Dinosaur Bone.\n");
        output.format(">");
        
        key=input.nextLine();
        
        switch(key)
        {
            case "1":
//                Create();
                done=true;
                break;
            case "2":
                if(loaded == 0)
                {
                    output.format("No bones exist.\n");
                }
                else
                { 
                    Update();
                }
                done=true;
                break;   
            case "3":
                Remove();
                done=true;
                break;                
                
                
            default:
                output.format("Please Select One.\n");
                break;
        
        }       
        
        }
    }
    
    public void Create(String new_name, int longitude, int latitude, float new_price, int new_ID) //*creates a bone
    {
        BoneNode tempNode;
        new_name = new_name;
        //int longitude;
        int new_long;
        //int latitude;
        int new_lad;
        //float new_price;
        int new_sold;
        int choice;
//        output.format("Please enter the name.\n");
//        new_name = input.next();
//        output.format("Please enter a price.\n");
//        new_price = input.nextInt();
//        output.format("Please enter the longitude.\n");
//        longitude = input.nextInt();
//        output.format("Please enter the latitude.\n");
//        latitude = input.nextInt();
//        output.format("Please enter the ID.\n");
//        new_ID = input.nextInt();
        if(new_ID < 1013 && new_ID > 1000)
        {
            output.format("ID Taken. Please enter another ID.\n");
            new_ID = input.nextInt();
        }
        input.nextLine();
        new_sold = 0;
        //*algebra to convert from actual coordinates to 60x20 coordinates
        longitude = longitude + 180;
        latitude = latitude*-1;
        latitude = latitude+90;
        new_long = longitude/6;
        new_lad = latitude/9;
        if(new_long == 60)
            new_long--;
        if(new_lad == 20)
            new_lad--;
        
        
        tempNode=new BoneNode(new_name,new_price,new_sold,new_long,new_lad,new_ID);
        boneArrayList.add(tempNode);
        loaded=1;
    }

    public void Update() //*initiates the option to update a bone
    {
        BoneNode tempNode;
        int listMax;
        int length;
        
        listMax=boneArrayList.size();
        //*prints out list of bones for user
        for(length=0;length<listMax;length++)
        {
            tempNode = boneArrayList.get(length);
            
            output.format("%s,", tempNode.name);
            output.format("%d\n", tempNode.ID);
        }
        String key;
        boolean done;
        
        done=false;
        
        while(!done)
        {
            output.format("Select a bone?\n");
            output.format("1.Input an ID.\n");
            output.format("2.Return to Menu.\n");
            output.format(">");
        
            key=input.nextLine();
            
            switch(key)
            {
                case "1":
                    BoneUpdate();
                    done=true;
                    break;
                case "2":
                    done=true;
                    
                default:
                    output.format("Please Select One.\n");
                    break;
            }
        }
        
    }
    
    public void BoneUpdate() //*updates a bone's coordinates and cost
    {
        BoneNode tempNode;
        int listMax;
        int length;
        int IDin;
        
        listMax=boneArrayList.size();
        
        output.format("Input the ID of the Desired Bone.\n");
        IDin = input.nextInt();
        input.nextLine();
        
        for(length=0;length<listMax;length++)
        {
            tempNode = boneArrayList.get(length);
            
            if(IDin == tempNode.ID)
            {
                output.format("Enter new longitude.\n");
                tempNode.lon = (input.nextInt()+180)/6;
                output.format("Enter new latitude.\n");
                tempNode.lad = ((input.nextInt()*-1)+90)/9;
                output.format("Enter new price.\n");
                tempNode.price = input.nextFloat();
                input.nextLine();
            }
        }
    }
    
    public void Remove() //*initiates the option to remove a bone
    {
        BoneNode tempNode;
        int listMax;
        int length;
        
        listMax=boneArrayList.size();
        //*prints out list of bones for user
        for(length=0;length<listMax;length++)
        {
            tempNode = boneArrayList.get(length);
            
            output.format("%s,", tempNode.name);
            output.format("%d\n", tempNode.ID);
        }
        String key;
        boolean done;
        
        done=false;
        
        while(!done)
        {
            output.format("Remove a bone?\n");
            output.format("1.Input an ID.\n");
            output.format("2.Return to Menu.\n");
            output.format(">");
        
            key=input.nextLine();
            
            switch(key)
            {
                case "1":
                    BoneRemoval();
                    done=true;
                    break;
                case "2":
                    //menu();
                    done=true;
                    
                default:
                    output.format("Please Select One.\n");
                    break;
            }
        }
    }
    
    public void BoneRemoval() //*removes a bone from the arraylist
    {
        BoneNode tempNode;
        int listMax;
        int length;
        int IDin;
        int rem = 0;
        
        listMax=boneArrayList.size();
        
        output.format("Input the ID of the Desired Bone.\n");
        IDin = input.nextInt();
        input.nextLine();
        
        for(length=0;length<listMax;length++)
        {
            tempNode = boneArrayList.get(length);
            
            if(IDin == tempNode.ID)
            {
                rem = length;
            }
        }
        boneArrayList.remove(rem);
    }
    
    public void Save() //*saves the current arraylist of bone data into a new textfile w/ Try/Catch Block
    {
        int listMax;
        BoneNode tempNode;
        int length;
        int width;
        
        listMax=boneArrayList.size();
        
        String filename;
        output.format("Enter the Bone List FileName: \n");
        filename= input.next();
        input.nextLine();
        
        File outputfile=new File(filename);
        
        
        try {
            try (Formatter fileWriter = new Formatter(outputfile)) {
                for (length=0;length<listMax;length++)
                {
                    tempNode=boneArrayList.get(length);
                    fileWriter.format("%s,", tempNode.name);
                    fileWriter.format("%.2f,", tempNode.price);
                    fileWriter.format("%d,", tempNode.sold);
                    fileWriter.format("%d,", tempNode.lon);
                    fileWriter.format("%d,", tempNode.lad);
                    fileWriter.format("%d\r\n", tempNode.ID);
                } 
            }
            
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DBST.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void SaveSeller(File outputfile) //*saves the current Seller into a new textfile
    {
        Seller tempNode;

        try {
            try (Formatter fileWriter = new Formatter(outputfile)) {
                tempNode=sellerArrayList.get(0);
                fileWriter.format("%f,", tempNode.lon);
                fileWriter.format("%f,", tempNode.lad);
                fileWriter.format("%d,", tempNode.rowIndex);
                fileWriter.format("%d,", tempNode.colIndex);
                fileWriter.format("%s\r\n", tempNode.name2);
            }
            
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DBST.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void Load() //*loads a textfile into an arraylist w/ Try/Catch Block
    {
        String filename;
        output.format("Enter the Bone List FileName: \n");
        filename= input.next();
        input.nextLine();
        File mapFile=new File(filename);
        String line=null;
        String dataset[];
        String name;
        float price;
        int sold;
        int lon;
        int lad;
        int ID;
        
        boneArrayList.clear();
        
        BoneNode tempNode;
        
        try
        (Scanner listReader = new Scanner(mapFile)) {
        
        
        while (listReader.hasNextLine())
        {
            line=listReader.nextLine();
            //Data here screen.format(line);
            dataset=line.split(",");
            name=String.valueOf(dataset[0]);
            price=Float.parseFloat(dataset[1]);
            sold=Integer.parseInt(dataset[2]);
            lon=Integer.parseInt(dataset[3]);
            lad=Integer.parseInt(dataset[4]);
            ID=Integer.parseInt(dataset[5]);
            tempNode=new BoneNode(name,price,sold,lon,lad,ID);
            boneArrayList.add(tempNode);
        }
        }
        catch (FileNotFoundException fnfE)
        {
            fnfE.printStackTrace();
        }
        loaded = 2;
    }
    
    public void LoadSeller( int colIndex, int rowIndex, double lon, double lad) //*loads a Seller into its arraylist
    {
        String filename;
        output.format("Enter the Seller FileName: \n");
        filename= input.next();
        input.nextLine();
        File mapFile=new File(filename);
        String line=null;
        String dataset[];
        String name2;
      

        boneArrayList.clear();
        
        Seller tempNode;
        
        try
        {
            try (Scanner listReader = new Scanner(mapFile)) {
                while (listReader.hasNextLine())
                {
                    line=listReader.nextLine();
                    //Data here screen.format(line);
                    dataset=line.split(",");
                    lon=Double.parseDouble(dataset[0]);
                    lad=Double.parseDouble(dataset[1]);
                    rowIndex=Integer.parseInt(dataset[2]);
                    colIndex=Integer.parseInt(dataset[3]);
                    name2=String.valueOf(dataset[4]);
                    tempNode=new Seller(lon,lad,rowIndex,colIndex,name2);
                    sellerArrayList.add(tempNode);
                }   }
        }
        catch (FileNotFoundException fnfE)
        {
            fnfE.printStackTrace();
        }
        loaded2 = 1;
    }
    
    public void SellerCreate(double new_lon,double new_lad,String new_name2) 
    {
        
        Seller tempNode;
        /*
        output.format("Please enter the longitude.\n");
        new_lon = input.nextInt();
        output.format("Please enter the latitude.\n");
        new_lad = input.nextInt();
        output.format("Please enter the name.\n");
        new_name2 = input.next();
         */
        int new_colIndex = (int)new_lon + 180;
        int new_rowIndex = (int)new_lad*-1;
        new_rowIndex = new_rowIndex+90;
        new_colIndex = new_colIndex/6;
        new_rowIndex = new_rowIndex/9;
        if(new_colIndex == 60)
            new_colIndex--;
        if(new_rowIndex == 20)
            new_rowIndex--;
        
        tempNode=new Seller(new_lon,new_lad,new_rowIndex,new_colIndex,new_name2);
        sellerArrayList.add(tempNode);
        loaded2=1;
        
                
    } //*Create a Seller
    
    public void SellerUpdate() 
    {
        Seller tempNode;

        tempNode = sellerArrayList.get(0);

        output.format("Enter new longitude.\n");
        tempNode.colIndex = (input.nextInt()+180)/6;
        output.format("Enter new latitude.\n");
        tempNode.rowIndex = ((input.nextInt()*-1)+90)/9;

        input.nextLine();
    } //*Update Seller Coordinates
    
//    public void SellerMenu() {
//        
//        String key2;
//        boolean done;
//        
//        done=false;
//        
//        while(!done)
//        {
//        output.format("What do you want to do?\n");
//        output.format("1. Create Seller.\n");
//        output.format("2. Update Location.\n");
//        output.format("3. Quit.\n");
//        output.format(">");
//        
//        key2=input.nextLine();
//        
//        switch(key2)
//        {
//            case "1":
//                SellerCreate();
//                done=true;
//                break;   
//            case "2":
//                SellerUpdate();
//                done=true;
//                break;   
//            case "3":
//                done=true;
//                break;
//                
//                
//            default:
//                output.format("Please Select One.\n");
//                break;
//        
//        
//        }       
//        
//        }
//} //*Seller Menu to create/update a Seller
    
    public void quick(String key)
    {
        String line=null;
        String dataset[];
        BoneNode tempNode;
        int lon;
        int lad;
        float price;
        int ID;
        int choice;
        
        Scanner listReader=new Scanner(key);
        
        line=listReader.nextLine();
        dataset=line.split(",");
        lon=Integer.parseInt(dataset[0]);
        lad=Integer.parseInt(dataset[1]);
        price=Float.parseFloat(dataset[2]);
        
        lon = lon + 180;
        lad = lad*-1;
        lad = lad+90;
        lon = lon/6;
        lad = lad/9;
        if(lon == 60)
            lon--;
        if(lad == 20)
            lad--;
        
        ID = 1063;
        
        output.format("Suggested price: $650. Stay with $9876.54?\n");
            output.format("1. Yes.\n");
            output.format("2. No.\n");
            choice = input.nextInt();
            if(choice == 1)
            {
                price = (float)9876.54;
            }
            else
            {
                price = 650;
            }
        
        tempNode=new BoneNode("Dakosaurus",price,0,lon,lad,ID);
        boneArrayList.add(tempNode);
        
        loaded = 1;
    } //*Quick Create a Dinosaur
    
    public void menu() //*opening menu that asks what you want to do
    {
        String key;
        boolean done = false;
         
        while(!done)
        {
        output.format("Welcome to JavaBeans Bone Wholesale!\n");
        output.format("What do you want to do?\n");
        output.format("1. Load the world map.\n");
        output.format("2. Handle a Dinosaur Bone.\n");
        output.format("3. Show the World Map with Dinosaur Bones.\n");
        output.format("4. Save Files.\n");
        output.format("5. Load Bones.\n");
        output.format("6. Load Seller.\n");
        output.format("7. Seller Menu.\n");
        output.format("8. Quit.\n");
        output.format(">");
        
        key=input.nextLine();
        
        switch(key)
        {
            case "1":
             //   LoadMap();
                break;   
            case "2":
                Handle();
                break;   
            case "3":
                if(loaded == 0 && loaded2 == 0)
                    showMap();
                else
                    showMap2();
                break;   
            case "4":
                if(loaded == 1)
                {
                    Save();
                    if(loaded2 == 1)
                 //     SaveSeller();
                        return;
                }
                else if(loaded2 == 1)
               //    SaveSeller();
                    return;
                else
                    output.format("No existing data to save.\n");
                break;    
            case "5":
                Load();
                break;      
            case "6":
              //  LoadSeller();
                break;
            case "7":
           //      SellerMenu();
                break;
            case "8":
                done=true;
                break;
                
                
            default:
                if(key.length() > 5)
                {
                    quick(key);
                    break;
                }       
                output.format("Please Select One.\n");
                break;
        
        
        }       
        
        }
        
    } 
    
    class MapNode {
    
    int row;
    int col;
    int land;
    
    public MapNode(int col, int row, int land)
    {
        this.row=row;
        this.col=col;
        this.land=land;
    }
    
} //*node that records data from the map
    
    class BoneNode {
        
    String name;
    float price;
    int sold;
    int lon;
    int lad;
    int ID;
    
    public BoneNode(String name, float price, int sold, int lon, int lad, int ID)
    {
        this.name=name;
        this.price=price;
        this.sold=sold;
        this.lon=lon;
        this.lad=lad;
        this.ID=ID;
    }
} //*node that records data from the bone textfile
    
    public class Seller {
    
    double lon;
    double lad;
    int rowIndex;
    int colIndex;
    String name2;
    
    public Seller(double lon, double lad, int rowIndex, int colIndex, String name2)
    {
        this.lon = lon;
        this.lad = lad;
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
        this.name2 = name2;
    }

        public String getName2() {
            return name2;
        }
    
} //*Seller Class/Constructor

    public class Continent {
        
    int colIndex;
    int rowIndex;
    int land;
    
    public Continent(int colIndex, int rowIndex, int land)
    {
        this.colIndex = colIndex;
        this.rowIndex = rowIndex;
        this.land = land;
    }
} //*Continent Constructor
    
    public static void main(String[] args) 
    {
        // TODO code application logic here
        DBST dbst=new DBST();
        MainGUI main = new MainGUI();
        main.setVisible(true);
        dbst.menu();
    }
}