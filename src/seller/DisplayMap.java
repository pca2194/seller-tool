/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seller;

/**
 *
 * @author Michelle
 */

import javax.swing.JPanel;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.awt.Graphics;
import java.awt.Dimension;

public class DisplayMap extends JPanel{
    BufferedImage image;
    
    public DisplayMap(File mapFile){
        try {
        image = ImageIO.read(mapFile);
        }
        catch (IOException e){
        }
    }
    
    @Override
    public void paint(Graphics g){
        super.paint(g);
        g.drawImage(image,0,0,null);
    }

    @Override  // TO DETECT WRONG SPELLING!
    public Dimension getPreferredSize(){
    /*
    public Dimension getPrefferedSize(){
    */
    if(image == null){
            return new Dimension(100, 100);
    }else{
    // A JPanel is an ImageObserver
    return new Dimension(image.getWidth(this), image.getHeight(this));
    }
    }
}
