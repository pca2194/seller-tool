/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
public class Giganotosaurus extends LandCarnivore //Name of class: Gigantosaurus. Data Members: method: poorLittleMe.
{

    public Giganotosaurus(Coordinate coordinate, double price, int ID, double groundSpeed) {
        super(coordinate, price, ID, groundSpeed);
        poorLittleMe();
    }

    public void poorLittleMe() //Function name: poorLittleMe. No parameters. No return values.
    {
        System.out.println("Bigger than T.Rex, but not as cool.");
    }
}
