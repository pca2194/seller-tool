/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */

public class AirCarnivore extends AirDinosaur //Name of class: AirCarnivore. Data members: teeth.
{

    int teeth;

    public AirCarnivore(Coordinate coordinate, double price, int ID, double wingspan, int teeth) {
        super(coordinate, price, ID, wingspan); // Inherits attributes from AirDinosaur.
        this.teeth = teeth;
    }

}
