/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
public class Dakosaurus extends SeaCarnivore //Name of class: Dakosaurus. Data members: none.
{

    public Dakosaurus(Coordinate coordinate, double price, int ID, int propulsion, Boolean salt_water) {
        super(coordinate, price, ID, propulsion, salt_water); // Inherits attributes from Sea Carnivore.
        super.determinePropulsion();
        boogie();
    }

    public void boogie() // Company selling boogie boards with a fake bite out.
    {
        System.out.println("I survived the Dako!");
    }
}
