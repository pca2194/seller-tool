/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author Michelle
 */
public class Coordinate {

    double lon;
    double lad;
    int rowIndex;
    int colIndex;

    public Coordinate(double lon, double lad, int rowIndex, int colIndex) {
        this.lon = lon;
        this.lad = lad;
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
    }

}
