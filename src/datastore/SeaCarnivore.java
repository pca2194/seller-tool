/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

import java.util.Scanner;

/**
 *
 * @author PaulAsyn
 */

public class SeaCarnivore extends SeaDinosaur //Name of class: SeaCarnivore. Data members: determinePropulsion, propulsionLabel.
{

    int typeOfPropulsion;
    String propulsionLabel;

    public SeaCarnivore(Coordinate coordinate, double price, int ID, int typeOfPropulsion, Boolean salt_water) {
        super(coordinate, price, ID, salt_water);
        determinePropulsion();
    }

    public String determinePropulsion() // Prompts for number of propulsions and sets propulsions.
    {
        Scanner key = new Scanner(System.in);
       
        String pro1 = "Flippers";
        String pro2 = "Tails";
        String pro3 = "Feet";
        String pro4 = "Flippers and Tails";
        String pro5 = "Flippers and Feet";
        String pro6 = "Tails and Feet";
        String pro7 = "Flippers, Tails, and Feet";

        System.out.println("Determine the propulsion method(s).");
        System.out.println("1. Flippers");
        System.out.println("2. Tails");
        System.out.println("3. Feet");
        System.out.println("4. Flippers and Tails");
        System.out.println("5. Flippers and Feet");
        System.out.println("6. Tails and Feet");
        System.out.println("7. Flippers, Tails, and Feet");
        typeOfPropulsion = key.nextInt();

        switch (typeOfPropulsion) {
            case 1:
                propulsionLabel = pro1;
                break;

            case 2:
                propulsionLabel = pro2;
                break;

            case 3:
                propulsionLabel = pro3;
                break;

            case 4:
                propulsionLabel = pro4;
                break;

            case 5:
                propulsionLabel = pro5;
                break;

            case 6:
                propulsionLabel = pro6;
                break;

            case 7:
                propulsionLabel = pro7;
                break;
        }
        return propulsionLabel;
    }
}
