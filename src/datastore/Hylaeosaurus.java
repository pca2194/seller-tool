/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

import java.util.Random;

/**
 *
 * @author PaulAsyn
 */
public class Hylaeosaurus extends LandHerbivore //Name of class: Hylaeosaurus. Data members: armorThicknessOnLeft, armorThicknessOnRight, rand1, rand2.
{
    int armorThicknessOnLeft;
    int armorThicknessOnRight;

    public Hylaeosaurus(Coordinate coordinate, double price, int ID, double gait) {
        super(coordinate, price, ID, gait);
        Random rand1 = new Random();
        Random rand2 = new Random();
        armorThicknessOnLeft = rand1.nextInt((3 - 1) + 1) + 1; //Returns a random integer in range 1-3
        armorThicknessOnRight = rand2.nextInt((3 - 1) + 1) + 1; //Returns a random integer in range 1-3
    }
}
