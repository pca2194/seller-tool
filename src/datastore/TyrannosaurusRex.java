/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;
import java.util.Random;
/**
 *
 * @author PaulAsyn
 */ 
    
 public class TyrannosaurusRex extends LandCarnivore //Name of class: TyrannosaurusRex. Data members: smellingRange, rand, level.
 //Name of class: TyrannosaurusRex. Data members: smellingRange, rand, level.
 //Name of class: TyrannosaurusRex. Data members: smellingRange, rand, level.
 //Name of class: TyrannosaurusRex. Data members: smellingRange, rand, level.
    {
        int smellingRange;
        
        public TyrannosaurusRex (Coordinate coordinate, double price, int ID, double groundSpeed, int smellingRange)
        {
            super (coordinate, price, ID, groundSpeed);
            this.smellingRange=smellingRange;
        }
        
        public int levelOfScary() //Function name: levelOfScary. No parameters. Method returns a random integer in range 1-5 to tell how scary the T-Rex is.
        {
            Random rand=new Random();
            int level=rand.nextInt((5-1)+1)+1;
            return level;
        }
    }