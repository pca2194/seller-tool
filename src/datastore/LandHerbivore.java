/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
public class LandHerbivore extends LandDinosaur //Name of class: LandHerbivore. Data members: gait.
{

    double gait;

    public LandHerbivore(Coordinate coordinate, double price, int ID, double gait) {
        super(coordinate, price, ID);
        this.gait = gait;
    }
}
