/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

import java.lang.String;
import java.util.Scanner;

/**
 *
 * @author PaulAsyn
 */
public class Pterodactyl extends AirCarnivore //Name of class: Pterodactyl. Data members: coconut.
{

    Boolean coconut;

    public Pterodactyl(Coordinate coordinate, double price, int ID, int wingspan, int teeth) {
        super(coordinate, price, ID, wingspan, teeth);
        carryACoconut();
    }

    public void carryACoconut() { // Prompts to ask if the Pterodactyl is holding a coconut. 
        Scanner key = new Scanner(System.in);
        int answer;
        System.out.println("Is this Pterodactyl carrying a coconut? (0/1)): ");
        answer = key.nextInt();
        if (answer > 0) {
            coconut = true;
        } else {
            coconut = false;
        }
    }
}
