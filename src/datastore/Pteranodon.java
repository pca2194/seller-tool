/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
public final class Pteranodon extends AirCarnivore //Name of class: Pteranadon. Data members: saddle, saddleCost.
{

    Boolean saddle;
    double saddleCost = 1000;

    public Pteranodon(Coordinate coordinate, double price, int ID, int wingspan, int teeth) {
        super(coordinate, price, ID, wingspan, teeth);
        specialOffer();
    }

    public void specialOffer() { // If a saddle is available add the price to the whole cost.
        if (saddle = true) {
            price = price + saddleCost;
        }
    }

}
