/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
public class Shastasaurus extends SeaCarnivore //Name of class: SeaCarnivore. Data members: shastaTeeth.
{

    static int shastaTeeth = 0;

    public Shastasaurus(Coordinate coordinate, double price, int ID, int propulsion, Boolean salt_water) {
        super(coordinate, price, ID, propulsion, salt_water);
        super.determinePropulsion();

    }
}
