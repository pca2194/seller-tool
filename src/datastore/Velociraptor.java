/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;
/**
 *
 * @author PaulAsyn
 */ 
   public class Velociraptor extends LandCarnivore //Name of class: Velociraptor. Data members: represents_Size, actualSize.
 //Name of class: Velociraptor. Data members: represents_Size, actualSize.
 //Name of class: Velociraptor. Data members: represents_Size, actualSize.
 //Name of class: Velociraptor. Data members: represents_Size, actualSize.
    {
        int represents_Size; //Integer attribute represents the size of the Velociraptor
        
        public Velociraptor (Coordinate coordinate, double price, int ID, double groundSpeed, int represents_Size)
        {
            super (coordinate, price, ID, groundSpeed);
            this.represents_Size=represents_Size;    
        }
        
        public String sizeOfVelociraptor (int represents_Size) //Function name: sizeOfVelociraptor. One Parameter: integer represents the size of the Velociraptor. Method returns a string to tell the actual size of the Velociraptor.
        {
            String actualSize=null; //Tells the actual size of the Velociraptor unlike the integer attribute represents_Size.
            switch (represents_Size)
            {
                case 1: actualSize="small"; break;
                case 2: actualSize="medium"; break;
                case 3: actualSize="large"; break;
            }
            return actualSize;
        }
    }    
    