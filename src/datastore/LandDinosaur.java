/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
public class LandDinosaur extends Dinosaur //Name of class: LandDinosaur. Data members: speed.
{

    double speed = 15.0; //Default speed of land dinosaurs

    public LandDinosaur(Coordinate coordinate, double price, int ID) {
        super(coordinate, price, ID);
    }
}
