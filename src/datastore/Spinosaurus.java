/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
  public class Spinosaurus extends LandCarnivore //Name of class: Spinosaurus. Data members: numSpinesInSailfin.
    {
        int numSpinesInSailfin;
        
        public Spinosaurus (Coordinate coordinate, double price, int ID, double groundSpeed, int numSpinesInSailfin)
        {
            super (coordinate, price, ID, groundSpeed);
            this.numSpinesInSailfin=numSpinesInSailfin;
        }
    }