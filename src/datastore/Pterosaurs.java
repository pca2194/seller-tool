/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */


    public class Pterosaurs extends AirCarnivore //Name of class: Pterosaurs. Data members: crest.
    {
        String crest;

        public Pterosaurs(Coordinate coordinate, double price, int ID, int wingspan, int teeth, String crest) 
        {
            super(coordinate, price, ID, wingspan, teeth);
            this.crest = crest;
        }
    }