/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
public class LandCarnivore extends LandDinosaur //Name of class: LandCarnivore. Data members: groundSpeed.
{

    double groundSpeed;

    public LandCarnivore(Coordinate coordinate, double price, int ID, double groundSpeed) {
        super(coordinate, price, ID);
        this.groundSpeed = groundSpeed;
    }
}
