/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastore;

/**
 *
 * @author PaulAsyn
 */
import java.util.Random;

public class Dinosaur //This is the superclass of all dinosaur bones. Name of class: Dinosaur. Data members: coordinate, price, ID.
{

    Coordinate coordinate;
    double price;
    int ID;

    public Dinosaur(Coordinate coordinate, double price, int ID) {
        this.coordinate = coordinate;
        this.price = price;
        this.ID = ID;
    }
}
